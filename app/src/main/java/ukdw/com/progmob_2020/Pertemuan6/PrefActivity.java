package ukdw.com.progmob_2020.Pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ukdw.com.progmob_2020.R;

public class PrefActivity extends AppCompatActivity {
    String isLogin = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pref);

        Button btnPreff = (Button)findViewById(R.id.btnPref);

        //deklarasi preferences
        SharedPreferences pref = PrefActivity.this.getSharedPreferences("pref_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();  //buat variabel editor

        //membaca preferences islogin apakah true atau false, jd if islogin true posisi login
        isLogin = pref.getString("isLogin","0");
        if(isLogin.equals("1")){
            btnPreff.setText("Logout");
            }else{
            btnPreff.setText("Login");
        }

        //pengisian preferences
        btnPreff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isLogin = pref.getString("isLogin","0");
                if(isLogin.equals("0")){
                    editor.putString("isLogin","1");
                    btnPreff.setText("Logout");
                }else{
                    editor.putString("isLogin","0");
                    btnPreff.setText("Login");
                }
                editor.commit();
            }
        });
    }
}