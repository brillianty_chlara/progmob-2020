package ukdw.com.progmob_2020;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class TrackerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracker);

        TextView txtTracker = (TextView)findViewById(R.id.textView1);

        Bundle b = getIntent().getExtras();
        String textTracker = b.getString("help_string");
        txtTracker.setText(textTracker);
    }
}