package ukdw.com.progmob_2020.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Dosen {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("nama")
    @Expose
    private String nama;

    @SerializedName("nidn")
    @Expose
    private String nidn;

    private String notelp;

    @SerializedName("alamat")
    @Expose
    private String alamat;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("foto")
    @Expose
    private String foto;

    @SerializedName("nidn_progmob")
    @Expose
    private String nidn_progmob;

    public Dosen(String nama, String nidn, String notelp) {
        this.nama = nama;
        this.nidn = nidn;
        this.notelp = notelp;
    }

    public Dosen(String id, String nama, String nidn, String notelp, String alamat, String email, String foto, String nidn_progmob) {
        this.id = id;
        this.nama = nama;
        this.nidn = nidn;
        this.notelp = notelp;
        this.alamat = alamat;
        this.email = email;
        this.foto = foto;
        this.nidn_progmob = nidn_progmob;
    }

    public Dosen(String nama, String nidn, String notelp, String alamat, String email, String foto, String nidn_progmob) {
        this.nama = nama;
        this.nidn = nidn;
        this.notelp = notelp;
        this.alamat = alamat;
        this.email = email;
        this.foto = foto;
        this.nidn_progmob = nidn_progmob;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNidn() {
        return nidn;
    }

    public void setNidn(String nim) {
        this.nidn = nidn;
    }

    public String getNotelp() {
        return notelp;
    }

    public void setNotelp(String notelp) {
        this.notelp = notelp;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getNidn_progmob() {
        return nidn_progmob;
    }

    public void setNidn_progmob(String nidn_progmob) {
        this.nidn_progmob = nidn_progmob;
    }
}
