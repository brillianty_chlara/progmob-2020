package ukdw.com.progmob_2020.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Matakuliah {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("nama_mk")
    @Expose
    private String nama_mk;

    @SerializedName("idmk")
    @Expose
    private String idmk;

    @SerializedName("idmk_progmob")
    @Expose
    private String idmk_progmob;

    public Matakuliah(String nama_mk, String idmk) {
        this.nama_mk = nama_mk;
        this.idmk = idmk;
    }

    public Matakuliah(String id, String nama_mk, String idmk, String idmk_progmob) {
        this.id = id;
        this.nama_mk = nama_mk;
        this.idmk = idmk;
        this.idmk_progmob = idmk_progmob;
    }

    public Matakuliah(String nama_mk, String idmk, String idmk_progmob) {
        this.nama_mk = nama_mk;
        this.idmk = idmk;
        this.idmk_progmob = idmk_progmob;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama_mk() {
        return nama_mk;
    }

    public void setNama_mk(String nama_mk) {
        this.nama_mk = nama_mk;
    }

    public String getIdmk() {
        return idmk;
    }

    public void setIdmk(String idmk) {
        this.idmk = idmk;
    }

    public String getIdmk_progmob() {
        return idmk_progmob;
    }

    public void setIdmk_progmob(String idmk_progmob) {
        this.idmk_progmob = idmk_progmob;
    }
}
